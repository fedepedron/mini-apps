# FFTXlib_rho 


## Origin
Quantum ESPRESSO 
git@gitlab.com:pddforks/fft-test.git 70b6a260

## Short description
generates a 3D data set in G space and transforms it back and forth permorming  a 3D FFT
Reproduces the operation of FFTXlib in RHO modality inside QE, one of the most recurring kernels

## Build instructions
How to build it.
Update the Makefile and execute 
`make all` 

FFTXlib from git@gitlab.com:max-centre/components/fftxlib7.git
qeMPlib from git@gitlab.com:max-centre/components/qemplib.git 

## Run instructions
mpirun -np 4 ./miniapp1.x < input > out  

The program prints out a the norm of the difference between initial and final 3D datasets
## Test cases description


Ideally three test cases, if relevant:
1. file input 
2. file input executed with mpi 
3. yet to be commited
