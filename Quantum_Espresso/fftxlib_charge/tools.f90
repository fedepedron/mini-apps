!------------------------------------------------------------------------
module tools
!------------------------------------------------------------------------
 !
 contains
 !
SUBROUTINE read_rhog ( filename, ngm_g, nspin, rho_g, mill_g, b1, b2, b3, gamma_only, ierr )
   !------------------------------------------------------------------------
   !! Read and distribute rho(G) from file  "filename".* 
   !! (* = dat if fortran binary, * = hdf5 if HDF5)
   !! Processor "root_in_group" reads from file, distributes to
   !! all processors in the intra_group_comm communicator 
      !
USE ISO_FORTRAN_ENV, ONLY : DP => REAL64
      !
      IMPLICIT NONE
      !
      CHARACTER(LEN=*), INTENT(IN)              :: filename
      !! name of file read (to which a suffix is added)
      INTEGER,          INTENT(OUT)              :: ngm_g
      INTEGER,          INTENT(OUT)              :: nspin
      !! read up to nspin components
      COMPLEX(dp), ALLOCATABLE, INTENT(INOUT)   :: rho_g(:)
      !! temporary check while waiting for more definitive solutions
      INTEGER, ALLOCATABLE, INTENT(INOUT)       :: mill_g(:,:)
      LOGICAL, OPTIONAL, INTENT(INOUT)             :: gamma_only
      !! if present, don't stop in case of open error, return a nonzero value
      INTEGER, OPTIONAL, INTENT(OUT)            :: ierr
      !
      REAL(dp), INTENT(OUT)                      :: b1(3), b2(3), b3(3)
      INTEGER                                    :: iun, ns, ig
      logical                                    :: gamma_only_ 
      !
      WRITE( *, *) '(inside read rhog)'
      !
      iun  = 4
      IF ( PRESENT(ierr) ) ierr = 0
      !
      !
      OPEN ( UNIT = iun, FILE = TRIM( filename ) // '.dat', &
           FORM = 'unformatted', STATUS = 'old', iostat = ierr )
      IF ( ierr /= 0 ) THEN
         ierr = 1
         GO TO 10
      END IF
      !
      ! ...  READING GAMMA_ONLY, NGM, SPIN
      !
      READ (iun, iostat=ierr) gamma_only_, ngm_g, nspin          
      !
      if (gamma_only_ ) then 
        gamma_only = .true.
      else 
        gamma_only = .false. 
      end if 
      PRINT *, gamma_only, ngm_g, nspin
      !
      IF ( ierr /= 0 ) THEN
         ierr = 2
         GO TO 10
      END IF
      !
      READ (iun, iostat=ierr) b1, b2, b3
      IF ( ierr /= 0 ) WRITE(*,*) 'NOT READING B1 B2 B3'
      !
10    CONTINUE 
      !
      ALLOCATE( rho_g(ngm_g) )
      ALLOCATE (mill_g(3,ngm_g))
      !
      READ (iun, iostat=ierr) mill_g(1:3,1:ngm_g)
      IF ( ierr /= 0 ) WRITE(*,*) 'NOT READING MILL_G'
      !
      DO ns = 1, nspin
         !
         READ (iun, iostat=ierr) rho_g(1:ngm_g)
         IF ( ierr /= 0 ) WRITE(*,*) 'NOT READING RHO_G'
         !
!         IF ( ierr > 0 ) CALL errore ( 'read_rhog','error reading file ' &
!              & // TRIM( filename ), 2+ns )
         !
         ! 
!         IF ( nspin_ == 2 .AND. nspin == 4 .AND. ns == 2) THEN 
!            DO ig = 1, ngm 
!               rho(ig, 4 ) = rho(ig, 2) 
!               rho(ig, 2 ) = cmplx(0.d0,0.d0, KIND = DP) 
!               rho(ig, 3 ) = cmplx(0.d0,0.d0, KIND = DP)
!            END DO
!         END IF
      END DO
      !
      CLOSE (UNIT = iun, status ='keep' )
      !
      RETURN
      !
END SUBROUTINE read_rhog
    !
    !
    !
SUBROUTINE write_rhog ( filename, b1, b2, b3, gamma_only, mill, rho, ecutrho )
      !------------------------------------------------------------------------
      !! Collects rho(G), distributed on "intra_group_comm", writes it
      !! together with related information to file "filename".*
      !! (* = dat if fortran binary, * = hdf5 if HDF5)
      !! Processor "root_in_group" collects data and writes to file
      !
      USE ISO_FORTRAN_ENV, ONLY : DP => REAL64
      !
      IMPLICIT NONE
      !
      CHARACTER(LEN=*), INTENT(IN) :: filename
      !! rho(G) is distributed over this group of processors
      REAL(8),         INTENT(IN) :: b1(3), b2(3), b3(3)
      !!  b1, b2, b3 are the three primitive vectors in a.u.
      INTEGER,          INTENT(IN) :: mill(:,:)
      !! Miller indices for local G-vectors
      !! G = mill(1)*b1 + mill(2)*b2 + mill(3)*b3
      LOGICAL,          INTENT(IN) :: gamma_only
      !! if true, only the upper half of G-vectors (z >=0) is present
      COMPLEX(8),      INTENT(IN) :: rho(:)
      !! rho(G) on this processor
      REAL(8),OPTIONAL,INTENT(IN) :: ecutrho
      !! cut-off parameter for G-vectors, only the one in root node is
      !! used, hopefully the same as in the other nodes.  
      !
      INTEGER                  :: ngm, nspin, ns
      INTEGER                  :: iun, ierr
      !
      ngm  = SIZE (rho, 1)
!      IF (ngm /= SIZE (mill, 2) ) &
!         CALL errore('write_rhog', 'inconsistent input dimensions', 1)
      !
      nspin= 1 !SIZE (rho, 2)
      iun  = 6
      !
      ! ... find out the global number of G vectors: ngm
      !
      ierr = 0
      !
      OPEN ( UNIT = iun, FILE = TRIM(filename)//'.dat', &
                FORM = 'unformatted', STATUS = 'unknown', iostat = ierr )
      IF ( ierr /= 0 ) WRITE(*,*) 'error in opening the output file'
      !
      !
      WRITE (iun, iostat=ierr) gamma_only, ngm, nspin
      IF ( ierr /= 0 ) WRITE(*,*) 'not writing gamma_only, ngm, nspin'
      !
      WRITE (iun, iostat=ierr) b1, b2, b3
      IF ( ierr /= 0 ) WRITE(*,*) 'not writing b1, b2, b3'
      !
      ! ... write mill_g
      !
      WRITE (iun, iostat=ierr) mill(1:3,1:ngm)
      IF ( ierr /= 0 ) WRITE(*,*) 'not writing mill_g'
      !
      ! ... write rho_g
      !
      DO ns = 1, nspin
         !
         WRITE (iun, iostat=ierr) rho(1:ngm)
         IF ( ierr /= 0 ) WRITE(*,*) 'not writing b1, b2, b3'
         !
      END DO
      !
      CLOSE (UNIT = iun, status ='keep' )
      !
      RETURN
      !
END SUBROUTINE write_rhog
    !
    !
    !
SUBROUTINE recips (a1, a2, a3, b1, b2, b3)
    !---------------------------------------------------------------------
    !
    !   This routine generates the reciprocal lattice vectors b1,b2,b3
    !   given the real space vectors a1,a2,a3. The b's are units of 2 pi/a.
    !
    !     first the input variables
    !
USE ISO_FORTRAN_ENV, ONLY : DP => REAL64
    !
    !
    implicit none
    !
    real(DP), INTENT (IN) :: a1 (3), a2 (3), a3 (3)
    REAL(DP), INTENT (OUT)  :: b1 (3), b2 (3), b3 (3)
   
    ! input: first direct lattice vector
    ! input: second direct lattice vector
    ! input: third direct lattice vector
    ! output: first reciprocal lattice vector
    ! output: second reciprocal lattice vector
    ! output: third reciprocal lattice vector
    !
    !   then the local variables
    !
    interface 
      function cross(u,v) result (w)
        import dp 
        implicit none 
        real(dp) :: w(3)
        real(dp),intent(in) :: u(3),v(3)
     end function cross
    end interface 
    real(DP) :: den
    !
    b1 = cross(a2,a3)
    b2 = cross(a3,a1)
    b3 = cross(a1,a2)
    den = dot_product(a1,b1)
    b1 = b1/den 
    b2 = b2/den 
    b3 = b3/den 
    return
    !
end subroutine recips
 !
!----------------------------------------------------------------------
end module tools
!----------------------------------------------------------------------
!
!

