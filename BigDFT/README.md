# Mini-apps existing for the BigDFT-suite.
There are presently two mini-apps that are interesting to understand the behavious of saliend operations in BigDFT code.

 * The `Fock` program:  calculates the Exact exchange operator on a given number of Kohn-Sham orbitals for a given size of the computational domain and choice of boundary conditions.
   This program is useful to understan the accelerated behaviour of Hybrid Functional calculations in BigDFT.
   It can be installend from the test suite of the Poisson Solver library of BigDFT (gitlab.com/lsim/psolver).
   Some of the recent data for the SyCL acceleration can be found in the results directory 

 * The `libconv` program: originally in the liborbs library, to be moved back to the libconv project. 
   This program tests the behaviour of the BigDFT convolutions and its acceleration on various version of the sources which are generated by BOAST.
   It is able to handle multiple precision calculations as well as different wavelet family.
   The program in its present version can be found in the liborbs repo (test suite), but it is under movement in the libconv github project of bighdft suite.
