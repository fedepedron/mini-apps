# Mini-apps for SIESTA

In SIESTA, the lion's share of the computation time is spent in the
solver stage. One of the most relevant kernels is the diagonalization,
which in SIESTA is implemented fully by calls to external
libraries. In particular, the ELPA library is used for its
acceleration features.

ELPA is one of the code deliverables of the NOMAD CoE. A number of
miniapps related to the operation of the different ELPA kernels have
been prepared by BSC in that context.

Beyond diagonalization, SIESTA is able to use other
electronic-structure solvers. Most interesting is the PEXSI method,
which exhibits several levels of parallelization and a favorable
scaling.

There is a need to understand the balance of performance between the
different methods, depending on the system size and dimensionality,
and on the characteristics of the computer used. Work on a
meta-miniapp that drives both methods is scheduled to start during an
upcoming MaX hackathon.
