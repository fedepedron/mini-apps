# MaX Mini-apps
This is the Materials Science Domain Specific Mini-apps repository.

# Template to be filled for mini-apps:

## Origin
From which application/code the mini-apps comes from.
Original repo + associated release/branch/commit.

## Short description
Present briefly the mini-app and extracted kernel.
Explain briefly why this is relevant/representative for this application.

## Build instructions
How to build it.
List of dependencies.

## Run instructions
How to execute it and get any relevant output as performance results, etc.
Please provide a methodology to validate the results.

## Test cases description
Short description of the test case.

Ideally three test cases, if relevant:
1. Single-node test case
2. Multi-node test case
3. Big multi-node test case
